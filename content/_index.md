##### What is OCON?

  * Real time control and simulation made easy
  * Model complex designs using a tree node architecture 
  * Cross platform communication using Protobuf and gRPC
  
##### Get started

  * Read the posts about [oconomy](post/oconomy/) and [oconsistency](post/oconsistency/)
  * Example: setting up a [pendulum](post/pendulum/) service
  * Check out the repository at: https://gitlab.com/oconomy/ocon
