## OCON

- Modular hierarchical controller
  - Controller tree made out of simple controller nodes
  - Optimal message binding between controller interfaces
  - Customizable methods and interfaces 
- Communication using Protobuf and gRPC
  - Service based controller design
  - Communication across different languages and platforms
  - Reflective and serializable C++ interfaces
- Services
  - Generic gRPC server
  - Real time scheduler
  - Monitor in real time using the web GUI
  
## Controller architecture


- Controller node
  - A controller node consists of user definable methods / interfaces:
	 - Methods: `Init()`, `InitCallback()`, `Update()`, `UpdateCallback()`.
     - Configurable data interfaces for message binding and data monitoring
	 - Configurable RPC methods 
- Optimal message binding
  - A message binding defines a source and target message
  - Message binding allows for communication between controller nodes.
  - Relevant bindings are always executed just in time, e.g. before calling the update and callback method.
    Optimal message binding ensures minimal added latency. 
- Hierarchical controller
  - Each controller node can have any number of children
  - Each step executes recursively throughout the controller tree, e.g. starting from the root node:
     - Execute relevant message bindings
	 - Call `Update()` method
	 - Repeat these steps for each child node
	 - Execute relevant message callback bindings
	 - Call `UpdateCallback()` method
