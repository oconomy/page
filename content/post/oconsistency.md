---
title: Oconsistency
subtitle: Consistent real time scheduling
date: 2018-12-07
---

Real time performance scheduling is all about consistently meeting deadlines, not about being the fastest.
This post shows how a consistent real time performance can be achieved using a Linux pre-empt kernel together with the OCON control library.
An experiment is set up using a Raspberry Pi 3 with the modified PREEMPT_RT kernel running a bicycle simulation (see [bicycle example](https://gitlab.com/oconomy/ocon/tree/master/examples/bicycle)).
Finally the real time results are discussed and future work is pointed out. 

### PREEMPT_RT 

The PREEMPT_RT patch basically ensures that tasks with a low priority are preempted such that real-time tasks can run with minimal added latency.
For this test I used a Raspberry Pi 3 and applied the PREEMPT_RT patch explained in [this](https://lemariva.com/blog/2018/07/raspberry-pi-preempt-rt-patching-tutorial-for-kernel-4-14-y) tutorial by Mauro Riva (https://lemariva.com).

After the patch (with took some hours) there is one more adjustment required; we need to make sure that the user has sufficient rights to run the OCON real time priority thread.
This can be achieved by adding the following line to `/etc/security/limits.conf`, which increases the max priority for user `<username>` to the required value of 80:
```
<username> - rtprio 80
```

### Protobuf and real time performance

A quick word about Protobuf and real time performance. 
When I started writing this framework real time performance was not the main priority.
Instead I wrote the framework with flexibility and ease of use in mind.
I think Protobuf offers a lot of this, but I also realize it might not have been the best choice for real time applications.
The reason for this is that Protobuf uses a lot of heap allocation under the hood, which might not be so apparent to the user. 
Fortunately, there are a few tricks to circumvent this problem:

* Use Protobuf Arena allocation: [link](https://developers.google.com/protocol-buffers/docs/reference/arenas)
* Avoid setting Protobuf strings during step execution. 
* Avoid serialization / deserialization during step execution. 

These tricks should help achieving a reasonable real time performance while working with Protobuf messages. 

### Bicycle simulator

Now that the environment is set up, lets have some fun and run the [bicycle example](https://gitlab.com/oconomy/ocon/tree/master/examples/bicycle) at real time.
From the root folder of the OCON repository run the following bash commands:

```  bash
$ mkdir -p cmake/build && cd cmake/build
$ cmake -DCMAKE_BUILD_TYPE=Release ../.. && make -j2
$ cmake --build . --target bicycle_main -- -j2
$ examples/bicycle/bicycle_main
```

The GUI can be run from any platform and can be configured to look for remote nodes.
In my case I added a remote node by editing `gui/ocon/gui/web/config.json`:
```
{
  "nodes": [
    {"name": "raspberry-pi-3", "ip": "192.168.178.24"}
  ]
}
```
Again, from the root folder of the OCON repository, 
the following commands were used to run the GUI:
```
$ cd gui
$ pub global activate webdev
$ webdev serve web:80
```

If all is well the Raspberry Pi should come on line and show the following information:

![Example image](/scheduler.png)

To verify that the priority thread is running, please check that the corresponding boolean is set to true (no value means false).
In addition the scheduler also yields information regarding execution time, maximum execution time, and number of overruns (if any). 

### Results 

The average execution time is about 0.17ms with some jitter spikes up to up to 0.5ms of execution time.
The application runs at an interval of 10ms so all is well within tolerances and no overruns occur.
The current max load is about 5 percent so there is plenty of room left to increase the update rate and model complexity. 

### Future work

The following areas could be investigated to increase the OCONSISTENCY:

* Investigate Mutex lock priority policies to avoid priority inversion.
* Assign dedicated core to real time thread.
* Experiment with memory locking to avoid memory allocation from the heap. 

Still with me here? Thanks for the read and feel welcome to OCONTRIBUTE!