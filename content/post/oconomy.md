---
title: Oconomy
subtitle: Goods and services of the OCON library
date: 2018-12-08
---
This post dives into some of the features and concepts of the OCON library and provides a simplified overview.
It is encouraged to have a look into the source code as well, where the [public headers](https://gitlab.com/oconomy/ocon/tree/master/include/ocon), [proto definitions](https://gitlab.com/oconomy/ocon/tree/master/proto), and [examples](https://gitlab.com/oconomy/ocon/tree/master/examples) would provide a good starting point.
In short, the library consists of two important classes; the OCON node and OCON server.
These classes are explained in more detail in the remainder of this post.

## OCON node

The OCON node acts as a virtual building block from which you can inherit and build upon.
An OCON node can have children, which effectively makes it a node tree data structure.
The hierarchy is determined on construction where the parent node is specified:
```
Node::Node(Node* parent, const std::string& name, const unsigned& multi_rate_factor = 1);
```
Needless to say, setting the parent to null defines the root node.
The names of children under the same node should be unique, so that every node has a unique path within the controller tree.
The multi rate factor is optional, and will make the node (and chidren) run at a multiple rate of the parent execution interval. 
This feature could be usefull to increase numerical stability by executing certain branches of the controller tree at a higher rate. 

### Interfaces

Interfaces are defined as Protobuf messages and can be registered with the following method:
```
void RegisterInterface(const std::string &name, Message* inferface);
```
Registering an interface allocates all sub messages and makes the interface and all sub-messages eligible for signal binding (explained in later section).
Registered interfaces are also accessible using generic message requests and are visible in the OCON web GUI. 

### Step algorithm

This section explains the recursive step algorithm in a little more detail.
The step algorithm lies at the heart of the controller hence it is good to have a rough understanding how it works.
Below a simplified version of the main execution algorithm with some comments:
```
void Node::ExecuteStep(const double& time_step)
{
    // Update all inputs of current node
    ExecuteSignalBindings(bindings_);
    
    // Execute step at a multiple of the parent frequency (default = 1)
    double time_step_multi_rate = time_step / multi_rate_factor_;
    for (unsigned i = 0; i < multi_rate_factor_; i++) {
    
        // Call virtual step method
        Step(time_step_multi_rate);
        
        // Execute step algorithm for all children
        for (auto& child: children_) {
            child->ExecuteStep(time_step_multi_rate);
        }
        
        // Update inputs which are coupled to descendant outputs
        ExecuteSignalBindings(bindings_callback_);
    }
    
    // Call virtual callback method.
    StepCallback();
}
```
Note that the step algorithm is recursive and also executes the step algorithm for all the children. A single step execution 
of the root node effectively updates the entire controller tree. 

### Signal binding

Signal binding provides the means to connect and exchange data between different OCON nodes within the same controller tree. 
A signal binding defines a source and target message and can be created with the following method:
```
void ConnectSignals(const Message& source, const Message& target);
```
By default signal bindings are assigned to the target node and hence executed just in time, e.g. before calling the update and callback method (see step algorithm section).
By doing so, bindings are optimized and there is no added latency due to outdated inputs. 
There is the option to disable optimization and make the current node the owner (e.g. executor) of the signal binding.
Check the connect signals method in [include/ocon/node.h](https://gitlab.com/oconomy/ocon/blob/master/include/ocon/node.h) for more detail. 

### Locking strategy

Each node has a mutex for protecting its shared resources.
Multiple threads might try to access the node in parallel, so having a proper locking strategy is of key importance. 
Typically these threads include:

* Real time thread: periodic execution of the ExecuteStep() method.
* Monitor thread: periodic access of node data interfaces.
* gRPC server thread: intermittent execution of remote procedure calls. 

The Step(), StepCallback(), Init(), InitCallback() methods are all executed under a recursive node guard, so no worries here. 
Since the lock for these methods is recursive, it means you are free to access child controller resources as well.
On the other hand, you are not allowed to access parent resources.
The image below illustrates the recursive lock concept:

![Example image](/recursive_lock.png)

Side note: this design forces some degree of modularity where deeper nodes are more confined.
This concept is known as OCONFINEMENT and makes a good case for unit testing on different levels. 
Of course, there might be some degree of coupling between adjacent nodes due to signal binding, so reality could be a little more complicated.
   
When defining custom RPCs accessing shared data resources it is required to implement a lock.
The snippet below is taken over from the [pendulum](https://gitlab.com/oconomy/ocon/tree/master/examples/pendulum) example:
``` 
grpc::Status GetState(grpc::ServerContext* context, const Empty* request, State* response) override {
    LockGuard guard(this);
    response->CopyFrom(*outputs_);
    return grpc::Status::OK;
};
```
Notice how a `LockGuard` is instantiated to safeguard the shared resource: `outputs_`. 
     
## OCON server
Now that the OCON node has been introduced, lets have a look into the OCON server.
An OCON server basically wraps an OCON node and provides real time scheduling and network communication methods.
Below an example of the available server configuration options: 
```
message ServerConfiguration {
  string name = 1;
  string host = 2;
  int32 port_websocket = 3;
  int32 port_grpc = 4;
  bool scheduler = 5;
  double time_step = 6;
}
```
Side note: an OCON node is also perfectly fine and usable without a server.

### Scheduler
The real time scheduler executes the step method at fixed time intervals. A simplified real time scheduler is 
provided in the code snippet below to illustrate the concept:
```
// start the loop
auto next_start_time = steady_clock::now();
while (server->thread_running_) {

    // set the next start time
    next_start_time += time_step;
    
    // execute update
    server->root_node_->ExecuteStep(time_step);
        
    // check for overrun or sleep till next start time
    if (next_start_time > steady_clock::now()) {
        std::this_thread::sleep_until(next_start_time);
    } else {
        std::cout << "Overrun detected" << std::endl;
        next_start_time = steady_clock::now();
    }
}
```
If all is well, the execution finished before the next scheduled execution time.
If not, an overrun occurs. 
Overruns should be prevented at all cost. See also the post about [Oconsistency](../oconsistency/), which talks about real time performance and the Linux PREEMPT_RT kernel.
One could say that real time scheduling is all about consistently meeting deadlines, not about being the fastest.

### gRPC server

The OCON server includes a [gRPC](https://grpc.io/) server, which runs at the specified host address and gRPC port.
This will serve all registered custom services, as well as the generic services defined in the [proto](https://gitlab.com/oconomy/ocon/blob/master/proto/)
folder. 

### Websocket server

A Boost Beast websocket server is used for server communication with the GUI.
On request; a complete package of all node interfaces, node generic messages, and node server generic messages are serialized to JSON and send 
to the server in one big blob. Typically the GUI is updated at around 20 Hz.
This feature is definitely not scalable in the current form, so it is recommended to keep the number of connections limited.
Most likely the websocket will be replaced by gRPC web in the future so don't let your services rely on this.

## Summary

Below a short summary of the main concepts explained in this post.

* OCON node
  * Virtual building block
  * Virtual methods
     * Init(), InitCallback(), Step(), StepCallback()
  * Two types of constructors
      * Root node
      * Child node: specifies parent node on initialization
  * Customizable interfaces, signal bindings, and services
  * Provides a number of generic methods and services
* OCON server
  * Wraps an OCON root node into an OCON server
  * Real time scheduler (optional)
  * gRPC server
  * Websocket server (for GUI communication)

Still with me here? Thanks for the read and feel welcome to OCONTRIBUTE!