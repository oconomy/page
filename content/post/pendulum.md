---
title: Pendulum service
subtitle: Hi, I am a pendulum, how may I help you?!
date: 2018-12-10
---
This post explains how to set up a pendulum service using the OCON library.
First, a pendulum node is set up, which later on is extended into a pendulum service.
The full code is also part of the examples and can be found [here](https://gitlab.com/oconomy/ocon/tree/master/examples/pendulum).

![](/pendulum.png)

In this example, the pendulum is modeled as a weightless rod of length \\(l\\) with a point mass \\(m\\) attached at the end.
Gravity is indicated with \\(g\\) and the angle of the pendulum with respect to the vertical axis is defined as \\(\theta\\).
Finally there is an input \\(u\\) exerting torque around the axis of rotation.
 
### Part 1: Creating a node

Lets first start with defining the interfaces in pendulum.proto:

```
syntax = "proto3";

import "google/protobuf/empty.proto"; // will be used later, when we define services

option cc_enable_arenas = true;

package pendulum;

message Inputs {
    double torque = 1; // [Nm]
}

message State {
    double theta = 1; // [rad]
    double theta_dot = 2; // [rad/s]
}

message Parameters {
    double m = 1; // mass [kg]
    double g = 2; // gravity [m/s^2]
    double l = 3; // length [m]
}
```
This will define 3 messages types which will later on be used as node interfaces.

Note; it is not possible to define custom default values in the proto 3 definitions, instead they always are initialized to default values, e.g. zeros for numbers, empty string, and so on.

The option `cc_enable_arenas` is required by the OCON node template and generally optimizes memory usage and improves performance.

Next, let's write the implementation (pendulum.cpp): 

```
#include <math.h>
#include <ocon/node_template.h>

#include "pendulum.pb.h" // auto generated code pendulum.proto

using namespace ocon;
using namespace pendulum;

class Pendulum : public NodeTemplate<Inputs, State, Empty, Parameters> {

public:
    explicit Pendulum(const std::string& name) : NodeTemplate(name) {}

    void Init() override {
        outputs_->Clear();
        parameters_->set_m(1.00);
        parameters_->set_g(9.81);
        parameters_->set_l(1.00);
    }

    void Step(const double& time_step) override {

        // map in
        auto theta = outputs().theta();
        auto theta_dot = outputs().theta_dot();
        const auto& u = inputs().torque();
        const auto& g = parameters_->g();
        const auto& m = parameters_->m();
        const auto& l = parameters_->l();

        // state integration (simple 1st order Euler method)
        theta_dot += (-g/l*sin(theta) + u/(m*pow(l, 2.))) * time_step;
        theta += theta_dot * time_step;

        // map out
        outputs_->set_theta(theta);
        outputs_->set_theta_dot(theta_dot);
    }
};
```
The pendulum node is derived from the OCON node template which provides an easy way to define; input, output, internal, and parameter interfaces through template arguments.
Notice that internals are not defined in this example, so an empty Protobuf message is used as placeholder.

The init method clears the state and sets parameters to some default values.
The step method is where the magic happens and contains a simple first order Euler state integration in this case.

### Part 2: Defining services

Let's rework the pendulum node a bit and expand it with services to allow for external applications to connect and interact with the pendulum. 
To achieve this, we start by appending some services to the proto definition:
``` 
service PendulumService {
    rpc SetInputs(Inputs) returns (google.protobuf.Empty);
    rpc GetState(google.protobuf.Empty) returns (State);
}
```
These services are then compiled using the proto compiler with grpc extension enabled and are included with:

```
#include "pendulum.grpc.pb.h"
```

Now the service is ready to be instantiated, registered, and its virtual methods to be implemented.
This is done by updating the pendulum implementation (pendulum.cpp):

```
class Pendulum : public 
        NodeTemplate<Inputs, State, Empty, Parameters>,
        PendulumService::Service
{
public:
    explicit Pendulum(Node* parent, const std::string& name) : NodeTemplate(parent, name),
    {
        RegisterService(this);
    }
    
    grpc::Status SetInputs(grpc::ServerContext* context, const Inputs* request, Empty* response) override {
        LockGuard guard(this);
        inputs_->CopyFrom(*request);
        return grpc::Status::OK;
    };

    grpc::Status GetState(grpc::ServerContext* context, const Empty* request, State* response) override {
        LockGuard guard(this);
        response->CopyFrom(*outputs_);
        return grpc::Status::OK;
    };
    ...
}
```
Note: when accessing a critical section one must take care to lock the thread.
The node `LockGuard(Node*)` method provides a scoped lock, similar to how `std::lock_guard` works.

### Part 3: Start up the server

Finally, the server is ready to be started:

```
int main () {

    // model
    Pendulum pendulum("MyPendulum");

    // server
    ServerConfiguration config;
    config.set_scheduler(true);
    config.set_time_step(0.01);
    Server server(&pendulum, config);

    // some time to play
    server.Run();
    while(true) {};
}
```
This will instantiate a pendulum and wrap it into a server.
The scheduler is enabled and the pendulum step algorithm will run at 100 Hz.

Enjoy your well deserved pendulum service!